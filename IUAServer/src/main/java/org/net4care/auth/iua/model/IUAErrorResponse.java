package org.net4care.auth.iua.model;

public class IUAErrorResponse extends IUAResponse {
	private String error;
	private String error_description;

	public IUAErrorResponse(String error, String description) {
		this.error = error;
		this.error_description = description;
	}

	public String getError() {
		return error;
	}
	
	public void setError(String error) {
		this.error = error;
	}
	
	public String getError_description() {
		return error_description;
	}
	
	public void setError_description(String error_description) {
		this.error_description = error_description;
	}
}
