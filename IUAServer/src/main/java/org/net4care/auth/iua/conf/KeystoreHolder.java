package org.net4care.auth.iua.conf;

import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;

import javax.annotation.PostConstruct;

import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

@Component
public class KeystoreHolder implements ResourceLoaderAware {
    private X509Certificate certificate;
    private PrivateKey key;
	private ResourceLoader resourceLoader;

	@Override
	public void setResourceLoader(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}
	
	@PostConstruct
	public void load() throws Exception {
        KeyStore keystore = java.security.KeyStore.getInstance("JKS");
        keystore.load(resourceLoader.getResource("classpath:keystore.jks").getInputStream(), "Test1234".toCharArray());

        certificate = (X509Certificate) keystore.getCertificate("selfsigned");
        key = (PrivateKey) keystore.getKey("selfsigned", "Test1234".toCharArray());
	}
	
    public X509Certificate getCertificate() {
        return certificate;
    }

    public PrivateKey getKey() {
        return key;
    }
}
