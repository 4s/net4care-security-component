package org.net4care.auth.iua.model;

import org.net4care.auth.iua.conf.TokenConfiguration;


public class JWTTokenHeader extends JWTTokenElement {
	private String alg;
	private String kid; // Key ID (used for key lifecycle management)
	
	public JWTTokenHeader() {
		this.alg = "RS256"; // RSASSA-PKCS-v1_5 SHA-256 digital signature
		this.kid = TokenConfiguration.getKeyIdentifier();
	}

	public String getAlg() {
		return alg;
	}

	public String getKid() {
		return kid;
	}
}
