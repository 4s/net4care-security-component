package org.net4care.auth.iua.controller;

import org.apache.log4j.Logger;
import org.net4care.auth.AuthModule;
import org.net4care.auth.errors.AuthenticationError;
import org.net4care.auth.errors.BlockedCredentialsError;
import org.net4care.auth.errors.IncorrectCredentialsError;
import org.net4care.auth.errors.UnknownUserError;
import org.net4care.auth.iua.conf.KeystoreHolder;
import org.net4care.auth.iua.conf.TokenConfiguration;
import org.net4care.auth.iua.model.AccessToken;
import org.net4care.auth.iua.model.IUAErrorResponse;
import org.net4care.auth.iua.model.IUAResponse;
import org.net4care.auth.iua.model.JWSSignature;
import org.net4care.auth.iua.model.JWTToken;
import org.net4care.auth.iua.model.JWTTokenHeader;
import org.net4care.auth.model.Credentials;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetAuthorizationTokenController {
	private static final Logger logger = Logger.getLogger(GetAuthorizationTokenController.class);
	
	@Autowired private KeystoreHolder keystore;
	@Autowired private AuthModule authModule;

	@RequestMapping(method=RequestMethod.POST, value="/token", produces="application/json", consumes="application/x-www-form-urlencoded")
	public ResponseEntity<IUAResponse> getAuthorizationToken(@RequestParam(value="grant_type") String grantType, @RequestParam(value="username") String username, @RequestParam(value="password") String password) throws Exception {
		logger.debug("calling getAuthorizationToken for user: " + username);
		
		if (!"password".equals(grantType)) {
			return new ResponseEntity<IUAResponse>(new IUAErrorResponse("invalid_request", "request parameter 'grant_type' is not valid. It should be 'password' not '" + grantType + "'."), HttpStatus.BAD_REQUEST);
		}

		try {
			Credentials creds = new Credentials(username, password);
			authModule.authenticateUser(creds);
		}
		catch (AuthenticationError ex) {
			logger.debug("Failure authentication", ex);
			
			if (ex instanceof BlockedCredentialsError) {
				// ok, so HTTP 423 (locked) is actually the wrong choice, but for now it will have to do
				return new ResponseEntity<IUAResponse>(new IUAErrorResponse("unauthorized", "Log ind fejlede. Bruger låst."), HttpStatus.LOCKED);
			}
			else if (ex instanceof IncorrectCredentialsError) {
				return new ResponseEntity<IUAResponse>(new IUAErrorResponse("unauthorized", "Log ind fejlede."), HttpStatus.UNAUTHORIZED);
			}
			else if (ex instanceof UnknownUserError) {
				return new ResponseEntity<IUAResponse>(new IUAErrorResponse("unauthorized", "Log ind fejlede."), HttpStatus.UNAUTHORIZED);
			}
			else {
				return new ResponseEntity<IUAResponse>(new IUAErrorResponse("unauthorized", "Log ind fejlede: " + ex.getClass().toString()), HttpStatus.UNAUTHORIZED);
			}
		}

		JWTToken jwtToken = new JWTToken(username, TokenConfiguration.getTokenAudience());
		JWTTokenHeader jwtTokenHeader = new JWTTokenHeader();
		JWSSignature jwsSignature = new JWSSignature(jwtTokenHeader, jwtToken, keystore.getKey());

		String jwtTokenEncoded = new String(jwtToken.getBase64UrlEncoded(), TokenConfiguration.getCharset());
		String jwtTokenHeaderEncoded = new String(jwtTokenHeader.getBase64UrlEncoded(), TokenConfiguration.getCharset());
		String jwsSignatureEncoded = new String(jwsSignature.getBase64UrlEncoded(), TokenConfiguration.getCharset());
		AccessToken accessToken = new AccessToken(jwtTokenHeaderEncoded + "." + jwtTokenEncoded + "." + jwsSignatureEncoded);

		// TODO: 200 for OK, and 219 for password must be changed! (not currently supported)
		return new ResponseEntity<IUAResponse>(accessToken, HttpStatus.OK);
	}
}
