package org.net4care.auth.iua.model;

import java.security.PrivateKey;
import java.security.Signature;

import org.apache.tomcat.util.codec.binary.Base64;
import org.net4care.auth.iua.conf.TokenConfiguration;

public class JWSSignature {
	private byte[] signature;
	
	public JWSSignature(JWTTokenHeader header, JWTToken body, PrivateKey key) throws Exception {
		byte[] encoded = (new String(header.getBase64UrlEncoded(), TokenConfiguration.getCharset()) + "." + new String(body.getBase64UrlEncoded(), TokenConfiguration.getCharset())).getBytes(TokenConfiguration.getCharset());

		//RSASSA-PKCS-v1_5 SHA-256 digital signature
		
		Signature signatureEngine = Signature.getInstance("SHA256withRSA");
		signatureEngine.initSign(key);
		signatureEngine.update(encoded);
		this.signature = signatureEngine.sign();
	}

	public byte[] getBase64UrlEncoded() {
		return Base64.encodeBase64URLSafe(signature);
	}
}
