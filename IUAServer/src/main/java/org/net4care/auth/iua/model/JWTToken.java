package org.net4care.auth.iua.model;

import java.util.UUID;

import org.net4care.auth.iua.conf.TokenConfiguration;

public class JWTToken extends JWTTokenElement {
	private String iss; // Issuer of token
	private String sub; // Subject of token
	private String aud; // Audience of token
	private long exp; // Expiration time
	private String jti; // JWT ID
	
	public JWTToken(String sub, String aud) {
		this.sub = sub;
		this.aud = aud;
		this.iss = TokenConfiguration.getTokenIssuer();
		this.exp = System.currentTimeMillis() / 1000 + TokenConfiguration.getTokenValidityInSeconds();
		this.jti = UUID.randomUUID().toString();
	}

	public String getIss() {
		return iss;
	}

	public String getSub() {
		return sub;
	}

	public String getAud() {
		return aud;
	}

	public long getExp() {
		return exp;
	}

	public String getJti() {
		return jti;
	}
}
