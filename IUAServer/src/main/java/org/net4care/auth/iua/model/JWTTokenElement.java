package org.net4care.auth.iua.model;

import org.apache.tomcat.util.codec.binary.Base64;
import org.net4care.auth.iua.conf.TokenConfiguration;

import com.google.gson.Gson;

public class JWTTokenElement {
	public byte[] getBase64UrlEncoded() {
		return Base64.encodeBase64URLSafe(new Gson().toJson(this).getBytes(TokenConfiguration.getCharset()));
	}
}
