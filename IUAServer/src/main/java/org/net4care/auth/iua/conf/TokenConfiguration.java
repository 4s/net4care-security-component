package org.net4care.auth.iua.conf;

import java.nio.charset.Charset;

public class TokenConfiguration {
	private static Charset charset = Charset.forName("UTF-8");
	
	public static long getTokenValidityInSeconds() {
		return 60 * 60 * 8; // 8 hours
	}
	
	public static String getTokenAudience() {
		return "OpenTele";
	}
	
	public static String getTokenIssuer() {
		return "IHE IUA Authorization Server";
	}
	
	public static String getKeyIdentifier() {
		return "TODO"; // TODO: some value, perhaps a serialnumber?
	}
	
	public static Charset getCharset() {
		return charset;
	}
}
