package org.net4care.auth.iua.conf;

import java.io.IOException;

import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11NioProtocol;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ResourceLoader;

@Configuration
@PropertySource(value="classpath:tomcat.properties")
public class ServletContainerConfiguration {
	@Value("${tomcat.port}")
	private int port;
	
	@Value("${tomcat.keystoreFile}")
	private String keystoreFile;
	
	@Value("${tomcat.keystorePassword}")
	private String keystorePassword;
	
	@Value("${tomcat.keystoreAlias}")
	private String keystoreAlias;
	
	@Value("${tomcat.truststoreFile}")
	private String truststoreFile;
	
	@Value("${tomcat.truststorePassword}")
	private String truststorePassword;

	@Autowired
	private ResourceLoader resourceLoader;
	
    @Bean
    public EmbeddedServletContainerFactory servletContainer() throws Exception {
    	TomcatEmbeddedServletContainerFactory tomcat = new TomcatEmbeddedServletContainerFactory();

    	tomcat.addConnectorCustomizers(new TomcatConnectorCustomizer() {
			
			@Override
			public void customize(Connector connector) {
				connector.setScheme("https");
				connector.setPort(port);
				connector.setProtocol("HTTP/1.1");
				connector.setSecure(true);

		    	String keystoreLocation = null;
				try {
					keystoreLocation = resourceLoader.getResource(keystoreFile).getFile().getAbsolutePath();
				}
				catch (IOException ex) {
					throw new RuntimeException(ex);
				}

				Http11NioProtocol proto = (Http11NioProtocol) connector.getProtocolHandler();
				proto.setSSLEnabled(true);
				proto.setKeystoreFile(keystoreLocation);
				proto.setKeystorePass(keystorePassword);
				proto.setKeyAlias(keystoreAlias);
				proto.setKeystoreType("PKCS12");
				proto.setKeystoreProvider("SunJSSE");
				proto.setMaxThreads(150);
			}
		});
    	
        return tomcat;
    }
}
