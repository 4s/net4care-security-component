package org.net4care.auth.iua.model;

public class AccessToken extends IUAResponse {
	public String access_token;
	public String token_type;
	
	public AccessToken(String accessToken) {
		this.access_token = accessToken;
		this.token_type = "IHE-JWT";
	}

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
	
	public String getToken_type() {
		return token_type;
	}
	
	public void setToken_type(String token_type) {
		this.token_type = token_type;
	}
}
