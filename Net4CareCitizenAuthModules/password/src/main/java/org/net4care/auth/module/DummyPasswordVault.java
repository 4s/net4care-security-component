package org.net4care.auth.module;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.net4care.auth.model.CredentialStatus;
import org.net4care.auth.model.Credentials;

public class DummyPasswordVault implements PasswordVault {

	@Override
	public List<Credentials> getCredentialsForUser(String userid) {
		ArrayList<Credentials> listOfCredentials = new ArrayList<Credentials>();
		
		Credentials credentials = new Credentials(userid, "Test1234");
		credentials.setStatus(CredentialStatus.VALID);
		credentials.setId(new Random().nextLong());
		listOfCredentials.add(credentials);
		
		return listOfCredentials;
	}

}
