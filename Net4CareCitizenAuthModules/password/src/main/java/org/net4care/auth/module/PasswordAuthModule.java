package org.net4care.auth.module;

import java.util.List;

import org.net4care.auth.AuthModule;
import org.net4care.auth.errors.AuthenticationError;
import org.net4care.auth.errors.IncorrectCredentialsError;
import org.net4care.auth.model.Credentials;
import org.springframework.stereotype.Component;

@Component
public class PasswordAuthModule implements AuthModule {
	private PasswordVault passwordVault = new DummyPasswordVault();
	
	@Override
	public void authenticateUser(Credentials credentials) throws AuthenticationError {
		List<Credentials> credentialsForUser = passwordVault.getCredentialsForUser(credentials.getUserid());
		for (Credentials credentials2 : credentialsForUser) {
			if (credentials2.getSecret().equals(credentials.getSecret())) {
				return; // credentials are valid, so do not throw any errors
			}
		}

		throw new IncorrectCredentialsError();
	}

	@Override
	public Credentials createUserCredentials(Credentials credentials) {
		// DUD
		return credentials;
	}

	@Override
	public void deleteCredentials(Credentials credentials) {
		// DUD
	}

	@Override
	public List<Credentials> listUserCredentials(long userid) {
		// DUD
		return null;
	}

	@Override
	public Credentials preAuthenticateUser(Credentials credentials) throws AuthenticationError {
		// DUD - TODO: should this throw an exception? Or is "null" the correct response?
		return null;
	}

	@Override
	public Credentials updateCredentials(Credentials credentials) {
		// DUD
		return credentials;
	}

}
