package org.net4care.auth.module;

import java.util.List;

import org.net4care.auth.model.Credentials;

public interface PasswordVault {
	List<Credentials> getCredentialsForUser(String userid);
}
