package org.net4care.auth.module;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.sound.sampled.UnsupportedAudioFileException;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.net4care.auth.AuthModule;
import org.net4care.auth.errors.AuthenticationError;
import org.net4care.auth.errors.IncorrectCredentialsError;
import org.net4care.auth.model.Credentials;
import org.springframework.stereotype.Component;

import com.bitsinharmony.recognito.MatchResult;
import com.bitsinharmony.recognito.Recognito;
import com.bitsinharmony.recognito.VoicePrint;

@Component
public class VoiceAuthModule implements AuthModule {
	private static final Logger logger = Logger.getLogger(VoiceAuthModule.class);
	
	//@Autowired private OpenTeleUserDao dao;
	static final Recognito<String> recognito;
	static {
		recognito = new Recognito<String>(16000.0f);
		try {
			VoicePrint printTest = recognito.createVoicePrint("test", new File("test.wav"));
			VoicePrint printHallo = recognito.createVoicePrint("hallo", new File("hallo.wav"));
			VoicePrint printMichael = recognito.createVoicePrint("Michael", new File("michael-passphrase.wav"));
			VoicePrint printNancyAnn = recognito.createVoicePrint("NancyAnn", new File("michael-passphrase.wav"));
			VoicePrint printBrian = recognito.createVoicePrint("Brian", new File("brian-passphrase.wav"));
			
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}		
		
	}
	
	@Override
	public void authenticateUser(Credentials credentials) throws AuthenticationError {
		logger.debug("authenticating user: " + credentials.getUserid());
			
		String username = credentials.getUserid();
		byte[] audioBytes = Base64.decodeBase64(credentials.getSecret());
		
		try{
		writeAudioToTempFile(audioBytes);
		convertToWav();
		}catch(Exception e){
			throw new IncorrectCredentialsError();
		}
		
		List<MatchResult<String>> matches;
		try {
			matches = recognito.identify(new File("temp.audio.wav"));
				MatchResult<String> match = matches.get(0);
			System.out.println("got key: "+match.getKey()+" with likelyhood : "+match.getLikelihoodRatio());
			if(!username.equals(match.getKey()) || match.getLikelihoodRatio() < 75){
				clearFiles();
				throw new IncorrectCredentialsError();
			}
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
			clearFiles();
			throw new IncorrectCredentialsError();
		} catch (IOException e) {
			e.printStackTrace();
			clearFiles();
			throw new IncorrectCredentialsError();
		}
		clearFiles();
		System.out.println("User successfully authenticated");
		logger.debug("User successfully authenticated");
	}

	private void convertToWav() throws Exception{
		Process process = Runtime.getRuntime().exec("ffmpeg -i temp.audio.file temp.audio.wav");
		process.waitFor();
		System.out.println("Done converting");
	}
	
	private void writeAudioToTempFile(byte[] bytes) throws Exception{
		FileOutputStream output;
		output = new FileOutputStream("temp.audio.file");
		output.write(bytes);
		output.flush();
		output.close();
	}
	
	@Override
	public Credentials createUserCredentials(Credentials credentials) {
		// DUD
		return credentials;
	}

	@Override
	public void deleteCredentials(Credentials credentials) {
		// DUD
	}

	@Override
	public List<Credentials> listUserCredentials(long userid) {
		// DUD
		return null;
	}

	@Override
	public Credentials preAuthenticateUser(Credentials credentials) throws AuthenticationError {
		// DUD - TODO: should this throw an exception? Or is "null" the correct response?
		return null;
	}

	@Override
	public Credentials updateCredentials(Credentials credentials) {
		// DUD
		return credentials;
	}
	
	private void clearFiles(){
		new File("temp.audio.wav").delete();
		new File("temp.audio.file").delete();
	}
	
}
