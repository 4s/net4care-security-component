package org.net4care.auth.module;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;
import org.net4care.auth.AuthModule;
import org.net4care.auth.errors.AuthenticationError;
import org.net4care.auth.errors.BlockedCredentialsError;
import org.net4care.auth.errors.IncorrectCredentialsError;
import org.net4care.auth.errors.UnknownUserError;
import org.net4care.auth.model.Credentials;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OpenTeleAuthModule implements AuthModule {
	private static final Logger logger = Logger.getLogger(OpenTeleAuthModule.class);
	
	@Autowired private OpenTeleUserDao dao;
	
	@Override
	public void authenticateUser(Credentials credentials) throws AuthenticationError {
		logger.debug("authenticating user: " + credentials.getUserid());
		
		// does the user exist?
		OpenTeleUser openTeleUser = dao.getOpenTeleUser(credentials.getUserid());
		if (openTeleUser == null) {
			logger.debug("Unknown user");
			throw new UnknownUserError();
		}
		
		// check for locked status and similar
		if (openTeleUser.isAccount_expired() || openTeleUser.isAccount_locked() || !openTeleUser.isEnabled() || openTeleUser.isPassword_expired()) {
			logger.debug("Blocked user");
			throw new BlockedCredentialsError();
		}

		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			logger.error("Failed to create a messageDigest", e);
			throw new UnknownUserError(); // perhaps a more technical error should be used here
		}

		byte[] digest = md.digest(credentials.getSecret().toLowerCase().getBytes(Charset.forName("UTF-8")));
		String password = new String(Hex.encodeHex(digest));

		if (!openTeleUser.getPassword().equals(password)) {
			logger.debug("Incorrect password");

			if (dao.increaseBadLoginAttempts(openTeleUser)) {
				throw new BlockedCredentialsError();
			}
			else {
				throw new IncorrectCredentialsError();
			}
		}
		
		if (openTeleUser.getBad_login_attempts() > 0) {
			dao.resetBadLoginAttempts(openTeleUser);
		}
		
		logger.debug("User successfully authenticated");
	}

	@Override
	public Credentials createUserCredentials(Credentials credentials) {
		// DUD
		return credentials;
	}

	@Override
	public void deleteCredentials(Credentials credentials) {
		// DUD
	}

	@Override
	public List<Credentials> listUserCredentials(long userid) {
		// DUD
		return null;
	}

	@Override
	public Credentials preAuthenticateUser(Credentials credentials) throws AuthenticationError {
		// DUD - TODO: should this throw an exception? Or is "null" the correct response?
		return null;
	}

	@Override
	public Credentials updateCredentials(Credentials credentials) {
		// DUD
		return credentials;
	}
}
