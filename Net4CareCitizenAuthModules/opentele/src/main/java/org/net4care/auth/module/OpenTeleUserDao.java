package org.net4care.auth.module;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class OpenTeleUserDao {
	private static final Logger logger = Logger.getLogger(OpenTeleUserDao.class);
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public boolean increaseBadLoginAttempts(OpenTeleUser user) {
		boolean locked = false;
		user.setBad_login_attempts(user.getBad_login_attempts() + 1);
		
		if (user.getBad_login_attempts() >= 3) {
			locked = true;
			user.setAccount_locked(true);
		}
		
		jdbcTemplate.update("UPDATE users SET bad_login_attemps = ?, account_locked = ? WHERE username = ?",
				new Object[] { user.getBad_login_attempts(), user.isAccount_locked(), user.getUsername()}
		);
		
		logger.debug("increaseBadLoginAttempts returned " + locked);
		
		return locked;
	}
	
	public void resetBadLoginAttempts(OpenTeleUser user) {
		user.setBad_login_attempts(0);
		
		jdbcTemplate.update("UPDATE users SET bad_login_attemps = ? WHERE username = ?",
				new Object[] { user.getBad_login_attempts(), user.getUsername()}
		);
	}

	public OpenTeleUser getOpenTeleUser(String username) {
		OpenTeleUser user = null;
		
		logger.debug("getOpenTeleUser: " + username);

		try {
			user = jdbcTemplate.queryForObject(
				"SELECT account_expired, account_locked, bad_login_attemps, enabled, password, password_expired, username FROM users WHERE username = ?",
				new Object[] { username },
				new RowMapper<OpenTeleUser>() {

					@Override
					public OpenTeleUser mapRow(ResultSet rs, int rowNum) throws SQLException {
						OpenTeleUser user = new OpenTeleUser();
						
						if (hasColumn(rs, "account_expired")) {
							user.setAccount_expired(rs.getBoolean("account_expired"));
						}
						if (hasColumn(rs, "account_locked")) {
							user.setAccount_locked(rs.getBoolean("account_locked"));
						}
						if (hasColumn(rs, "bad_login_attemps")) {
							user.setBad_login_attempts(rs.getInt("bad_login_attemps"));
						}
						if (hasColumn(rs, "enabled")) {
							user.setEnabled(rs.getBoolean("enabled"));
						}
						if (hasColumn(rs, "password")) {
							user.setPassword(rs.getString("password"));
						}
						if (hasColumn(rs, "password_expired")) {
							user.setPassword_expired(rs.getBoolean("password_expired"));
						}
						if (hasColumn(rs, "username")) {
							user.setUsername(rs.getString("username"));
						}
						
						return user;
					}
					
				}
			);
		}
		catch (EmptyResultDataAccessException ex) {
			;
		}
		
		logger.debug("found: " + user);
		
		return user;
	}
	
	private boolean hasColumn(ResultSet rs, String columnName) throws SQLException {
		ResultSetMetaData rsmd = rs.getMetaData();
	    int columns = rsmd.getColumnCount();

	    // it really starts from 1
	    for (int i = 1; i <= columns; i++) {
	        if (columnName.equals(rsmd.getColumnName(i))) {
	            return true;
	        }
	    }

	    return false;
	}
}
