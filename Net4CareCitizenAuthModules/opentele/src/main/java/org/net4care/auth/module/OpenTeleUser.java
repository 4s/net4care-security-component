package org.net4care.auth.module;

public class OpenTeleUser {
	private boolean account_expired, account_locked, enabled, password_expired;
	private String username, password;
	private int bad_login_attempts;
	
	public boolean isAccount_expired() {
		return account_expired;
	}
	
	public void setAccount_expired(boolean account_expired) {
		this.account_expired = account_expired;
	}
	
	public boolean isAccount_locked() {
		return account_locked;
	}
	
	public void setAccount_locked(boolean account_locked) {
		this.account_locked = account_locked;
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean isPassword_expired() {
		return password_expired;
	}
	
	public void setPassword_expired(boolean password_expired) {
		this.password_expired = password_expired;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public int getBad_login_attempts() {
		return bad_login_attempts;
	}
	
	public void setBad_login_attempts(int bad_login_attempts) {
		this.bad_login_attempts = bad_login_attempts;
	}
}
