package org.net4care.auth.module.conf;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataSourceConfiguration {

	@Bean
	public DataSource dataSource() {
		org.apache.tomcat.jdbc.pool.DataSource dataSource = new org.apache.tomcat.jdbc.pool.DataSource();		
		
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setPassword("opentele");
		dataSource.setUsername("opentele");
		dataSource.setUrl("jdbc:mysql://localhost:3306/opentele");
		dataSource.setMaxActive(100);
		dataSource.setMaxIdle(50);
		dataSource.setInitialSize(10);
		dataSource.setTestWhileIdle(true);
		dataSource.setValidationQuery("SELECT 1");
		dataSource.setValidationInterval(60000);
		dataSource.setMaxAge(3600000);

		return dataSource;
	}
}
