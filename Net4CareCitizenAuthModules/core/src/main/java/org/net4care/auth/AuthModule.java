package org.net4care.auth;

import java.util.List;

import org.net4care.auth.errors.AuthenticationError;
import org.net4care.auth.model.Credentials;

public interface AuthModule {
	// id:         null        / <present in output>
	// userid:     <mandatory> / <present in output>
	// secret:     <optional>  / <present in output>
	// type:       <mandatory> / <present in output>
	// status:     null        / <present in output>
	// challenge:  null        / null
	public Credentials createUserCredentials(Credentials credentials);

	// id:         <mandatory> / <present in output>   // must be existing record
	// userid:     null        / <present in output>
	// secret:     <optional>  / <present in output>   // should be a new value (but could be the same)
	// type:       null        / <present in output>
	// status:     <optional>  / <present in output>   // should be a new value (but could be the same)
	// challenge:  null        / null
	public Credentials updateCredentials(Credentials credentials);

	// id:         <mandatory>   // must be existing record
	// userid:     null
	// secret:     null
	// type:       null
	// status:     null
	// challenge:  null
	public void deleteCredentials(Credentials credentials);

	// returns a list of credentials for the user, with the following information
	// id:         <present in output>
	// userid:     <present in output>
	// secret:     null
	// type:       <present in output>
	// status:     <present in output>
	// challenge:  null	
	public List<Credentials> listUserCredentials(long userid);
	
	// used as the 1st call in a 2-step authentication scheme
	// id:         null        / <present in output>
	// userid:     <mandatory> / <present in output>   // together with "type" this should match at least 1 entry
	// secret:     <optional>  / null                  // (e.g. null for smspasscode and password in username/password schemes)
	// type:       <mandatory> / <present in output>   // together with "userid" this should match at least 1 entry
	// status:     null        / null
	// challenge:  null        / <might be present>    // for challenge based schemes, this contains the challenge
	public Credentials preAuthenticateUser(Credentials credentials) throws AuthenticationError;
	
    // used as the 2nd call in 2-step authentication schemes, and as the only call in 1-step schemes
	// id:         null        / <present in output>
	// userid:     <mandatory> / <present in output>   // together with "type" this should match at least 1 entry
	// secret:     <mandatory> / null                  // the secret to be verified - must be correct or an error is thrown
	// type:       <mandatory> / <present in output>   // together with "userid" this should match at least 1 entry
	// status:     null        / null
	// challenge:  null        / null
	public void authenticateUser(Credentials credentials) throws AuthenticationError;
}
