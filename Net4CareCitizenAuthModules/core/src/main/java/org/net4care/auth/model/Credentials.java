package org.net4care.auth.model;

public class Credentials {
	private long id;
	private String userid;
	private String secret;
	private CredentialStatus status;
	private String challenge;
	
	public Credentials(String userid, String secret) {
		this.userid = userid;
		this.secret = secret;
	}

	public Credentials(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getUserid() {
		return userid;
	}
	
	public void setUserid(String userid) {
		this.userid = userid;
	}
	
	public String getSecret() {
		return secret;
	}
	
	public void setSecret(String secret) {
		this.secret = secret;
	}
		
	public CredentialStatus getStatus() {
		return status;
	}
	
	public void setStatus(CredentialStatus status) {
		this.status = status;
	}
	
	public String getChallenge() {
		return challenge;
	}
	
	public void setChallenge(String challenge) {
		this.challenge = challenge;
	}
}
