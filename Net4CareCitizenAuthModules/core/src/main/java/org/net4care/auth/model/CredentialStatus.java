package org.net4care.auth.model;

public enum CredentialStatus {
	VALID,
	BLOCKED
}
