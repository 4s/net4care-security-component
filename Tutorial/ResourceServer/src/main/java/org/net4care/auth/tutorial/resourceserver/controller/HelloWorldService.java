package org.net4care.auth.tutorial.resourceserver.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldService {
	@RequestMapping(method=RequestMethod.GET, value="/hello")
	public String getHelloWorld(){
		return "Hello World";
	}
}
