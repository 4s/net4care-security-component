package org.net4care.auth.tutorial.resourceserver.iua;

import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Base64;

import com.google.gson.Gson;

public class AuthorizationTokenElement {
    public byte[] getBase64UrlEncoded() {
        byte[] raw = new Gson().toJson(this).getBytes(Charset.forName("UTF-8"));
        return Base64.encodeBase64URLSafe(raw);
    }
}
