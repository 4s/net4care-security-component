package org.net4care.auth.tutorial.resourceserver.iua;

import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.security.Signature;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.google.gson.Gson;

public class AuthorizationTokenValidator {
	private static final Logger logger = Logger.getLogger(AuthorizationTokenValidator.class);
    private static X509Certificate certificate = null;
	private Gson gson = new Gson();
    
    static {
    	String cert = "MIIDlzCCAn+gAwIBAgIENdp6yDANBgkqhkiG9w0BAQsFADB8MQswCQYDVQQGEwJESzEQMA4GA1UECBMHSnlsbGFuZDEPMA0GA1UEBxMGQWFyaHVzMRMwEQYDVQQKEwpIZWFsdGhjYXJlMREwDwYDVQQLEwhTb2Z0d2FyZTEiMCAGA1UEAxMZQWxleGFuZHJhIEluc3RpdHV0dGV0IEEvUzAeFw0xNDA1MjAwODQzMDZaFw0yNDA1MjAwODQzMDZaMHwxCzAJBgNVBAYTAkRLMRAwDgYDVQQIEwdKeWxsYW5kMQ8wDQYDVQQHEwZBYXJodXMxEzARBgNVBAoTCkhlYWx0aGNhcmUxETAPBgNVBAsTCFNvZnR3YXJlMSIwIAYDVQQDExlBbGV4YW5kcmEgSW5zdGl0dXR0ZXQgQS9TMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq/XhJ6y9tvRydey9uKpO4i4Cflsmbo291BBM44RS77TVGz1/Tyamb2+y36W8v4NM2bc6Oh4hDimhOMr9nvFXy1/nW6XLFErmCj99W+bummNImjXGT3PwIhpzd0lsMhL/hkH+Y2Bik2nc21oeRQ1H7oglZGal2JMDp5qU9cez/7k49S6QbX6ZI88nzZpNwGpXq6/2ANK9ITHTMiVpgD6K1mKYmyZzXUnMIUibiQ5WKMD+aTByoKVSl2ww9Ez/wfQOocpoNaLOiwr7W2z/GXSe/QojSOqOYU4QdsWb/JwZ8qNaV66WXeUyjQOFs9wq+t6qdvx2AMYEz48HHG3jc0pf/wIDAQABoyEwHzAdBgNVHQ4EFgQU3fmyQ68tajtyUmu/g1IsH/CYuxswDQYJKoZIhvcNAQELBQADggEBAJjoReeYWgPZvDZiZ36HVO5HAlk80Py+plH2j9cKXs/68pj0mLxYBy16UVT7jFk9ybVyNvTa1ekjEwwaUwku/5Lc5iVqX0Dh8t8PPUCy5OQoK2Ob4C60XsOC3FtlelVMcGHB7umLt67H/Cnf3lBdu6QqCnswRsM9c6IzLHu7DYES9OR9zRqbiT+RYUlv/KGHURoU5LRI0N4YwXCCQXA+iECrTYYyWEI6ZPRaauVmjR+f6rIytzz8NeX4/qfEIBcNTXw7CN5lDUr3PFRQyS8kZuKSKUtVZJMTE0/2wXSvK+ddPqvD9spNr7Mdrwdj31TQmnc2vrd/jqADNXAKMW+0TPc=";
    	
    	try {
			byte[] raw = Base64.decodeBase64(cert);
			ByteArrayInputStream bis = new ByteArrayInputStream(raw);
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			certificate = (X509Certificate) cf.generateCertificate(bis);
    	}
    	catch (Exception ex) {
    		logger.error(ex);
    	}
    }
    
    public boolean validateToken(String claimedUsername, String token) throws Exception {
        String[] parts = token.split("\\.");
        String headerJSON = new String(Base64.decodeBase64(parts[0]));
        String bodyJSON = new String(Base64.decodeBase64(parts[1]));
        AuthorizationTokenHeader header = gson.fromJson(headerJSON, AuthorizationTokenHeader.class);
        AuthorizationTokenBody body = gson.fromJson(bodyJSON, AuthorizationTokenBody.class);

        if (!"RS256".equals(header.alg)) {
        	logger.error("unsupported algorithm: " + header.alg);
        	return false;
        }
        
        Signature sig = Signature.getInstance("SHA256withRSA");
        sig.initVerify(certificate);
        sig.update((parts[0] + "." + parts[1]).getBytes(Charset.forName("UTF-8")));
        if (!sig.verify(Base64.decodeBase64(parts[2]))) {
        	logger.error("bad signature");
            return false;
        }
 
        if (System.currentTimeMillis() > body.exp * 1000) {
        	logger.error("expired token");
            return false;
        }
 
        if (!"Tutorial".equals(body.aud)) {
        	logger.error("token not issued for Tutorial");
            return false;
        }
 
        if (!claimedUsername.equalsIgnoreCase(body.sub)) {
        	logger.error("subject in token does match claimed identity");
            return false;
        }
 
        return true;
    }
}
