package org.net4care.auth.tutorial.resourceserver.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.net4care.auth.tutorial.resourceserver.iua.AuthorizationTokenValidator;
import org.springframework.stereotype.Component;

@Component
public class SimpleAuthorizationTokenFilter implements Filter {

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletResponse response = (HttpServletResponse) res;
		HttpServletRequest request = (HttpServletRequest) req;
		boolean validToken = false;
		
		String authorizationHeader = request.getHeader("Authorization");
		if (authorizationHeader != null && authorizationHeader.length() > 0) {
			String payload = authorizationHeader.split(" ")[1]; // strip "Basic "
			String header = new String(Base64.decodeBase64(payload));
			String parts[] = header.split(":");
			
			try {
				validToken = new AuthorizationTokenValidator().validateToken(parts[0], parts[1]);
			}
			catch (Exception ex) {
				throw new RuntimeException(ex);
			}
		}
		
		if (!validToken) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			return;
		}

		chain.doFilter(req, res);
	}

	@Override
	public void init(FilterConfig filterConfig) {}

	@Override
	public void destroy() {}

}
