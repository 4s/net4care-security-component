package org.net4care.auth.tutorial.resourceserver.iua;

public class AuthorizationTokenHeader extends AuthorizationTokenElement {
    public String alg;
    public String kid;
}
