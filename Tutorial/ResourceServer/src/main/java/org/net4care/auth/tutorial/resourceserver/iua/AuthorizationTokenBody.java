package org.net4care.auth.tutorial.resourceserver.iua;

public class AuthorizationTokenBody extends AuthorizationTokenElement {
    public String iss;
    public String sub;
    public String aud;
    public long exp;
    public String jti;
}
