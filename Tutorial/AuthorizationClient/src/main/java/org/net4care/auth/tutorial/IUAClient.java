package org.net4care.auth.tutorial;

import java.io.IOException;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;

public class IUAClient {

	public static final String IUA_ENDPOINT = "http://127.0.0.1:8090/token";
	public static final String APPLICATION_ENDPOINT = "http://127.0.0.1:8081/hello";
	public static final String USERNAME = "MrTest";
	public static final String PASSWORD = "Test1234";
	
	private String token;
	
	public static void main(String[] args) {
		IUAClient client = new IUAClient();
		System.out.println("Getting access token from IUA Server:\n");
		client.getTokenFromIUAServer();
		System.out.println("\n\nUsing accesstoken to connect to protected resource server:\n");
		client.sendMessageToApplicationServer();
	}

	private void sendMessageToApplicationServer() {
        HttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(APPLICATION_ENDPOINT);

        httpGet.setHeader("Content-Type",
                "application/x-www-form-urlencoded;charset=UTF-8");
        
        String encoding = new String(Base64.encodeBase64((USERNAME+":"+this.token).getBytes()));
        httpGet.setHeader("Authorization", "Basic " + encoding);
        System.out.println(">HTTP GET to: "+APPLICATION_ENDPOINT);
        for(Header h: httpGet.getAllHeaders()){
        	System.out.println(">Header: "+h.getName()+" : "+h.getValue());	
        }

        try {
			HttpResponse response = httpclient.execute(httpGet);
			System.out.println("RESPONSE code: "+response.getStatusLine().getStatusCode());
			if (response.getStatusLine().getStatusCode() == 200){
				String body = EntityUtils.toString(response.getEntity());
				System.out.println(body);
			}
		} catch (IOException e) {
			System.out.println("Could not connect to resource server.");
		}
	}

	public boolean getTokenFromIUAServer(){
        HttpClient httpclient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(IUA_ENDPOINT);

        HttpEntity entity = new StringEntity("grant_type=password&username="+USERNAME+"&password="+PASSWORD, "UTF-8");
        httpPost.setEntity(entity);
        httpPost.setHeader("Content-Type",
                "application/x-www-form-urlencoded;charset=UTF-8");
        System.out.println(">HTTP POST to: "+APPLICATION_ENDPOINT);
        for(Header h: httpPost.getAllHeaders()){
        	System.out.println(">Header: "+h.getName()+" : "+h.getValue());	
        }
        try {
			System.out.println(">Body: "+EntityUtils.toString(entity));
		} catch (ParseException | IOException e1) {
			e1.printStackTrace();
		}

        HttpResponse response = null;
        try {
			response = httpclient.execute(httpPost);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Response code: "+response.getStatusLine().getStatusCode());
        switch(response.getStatusLine().getStatusCode()){
        case 401:
            System.out.println("Invalid username/password.");
            return false;
        case 423:
        	System.out.println("Account locked.");
            return false;
        case 200:
            System.out.println("POST succesful.");
            break;
        default:
        	System.out.println("Other error.");
            return false;
        }
        String rawToken;
		try {
			rawToken = EntityUtils.toString(response.getEntity());
			System.out.println("Response body: "+rawToken);
		} catch (ParseException| IOException e) {
			e.printStackTrace();
			return false;
		}
		this.token = getAccessToken(rawToken);
		return true;
	}
	
	private String getAccessToken(String rawToken){
		Gson gson = new Gson();
		RawToken token = gson.fromJson(rawToken, RawToken.class);
		return token.getAccess_token();
	}
	
	private class RawToken{
		private String access_token;
		private String token_type;

		public String getAccess_token() {
			return access_token;
		}/*
	//	public void setAccess_token(String access_token) {
	//		this.access_token = access_token;
	//	}
		public String getToken_type() {
			return token_type;
		}
		public void setToken_type(String token_type) {
			this.token_type = token_type;
		}*/
	}
	
}
